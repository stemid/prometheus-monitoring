# Prometheus kustomization

Kubernetes kustomization for Prometheus, personal use.

## Notes for anyone brave enough to use this

* Due to kustomize issues the kube-monitoring namespace is hard coded into ``kustomize/base/rbac.yaml`` in ``ClusterRoleBinding.subjects.namespace``, you can always override rbac.yaml in an overlay.
* To avoid using the default ServiceAccount rbac.yaml sets up the prometheus ServiceAccount instead and gives it permission to list resources in the cluster.
